﻿using VendingMachine.Application;
using VendingMachine.Application.Beverages;
using VendingMachine.Application.Coins;
using VendingMachine.Application.Interfaces;

var orangeJuice = new NamedBeverage("orangejuice");
var appleJuice = new NamedBeverage("applejuice");

IVendingMachine machine = new DefaultVendingMachine();

machine.RegisterBeverage(orangeJuice, 120);
machine.RegisterBeverage(appleJuice, 150);

machine.AddBeverages(orangeJuice, orangeJuice, appleJuice);

machine.AddCoins(
    new FiftyCoin(),
    new TenCoin(),
    new TenCoin(),
    new TenCoin(),
    new TenCoin(),
    new TenCoin(),
    new TenCoin(),
    new TenCoin()
    );

var (bev, change) = machine.Buy(orangeJuice, new TwoHundredCoin());

Console.WriteLine($"Got a {bev.GetIdentifier()} beverage and {change.Count()} coins as change");
Console.WriteLine($"Total change: {change.Sum(x => x.GetValueInCents())} cents");

