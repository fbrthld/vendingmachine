using NUnit.Framework;
using VendingMachine.Application.Beverages;

namespace VendinMachineTests;

[TestFixture]
public class BeverageTests
{
    [Test]
    public void TwoNamedBeveragesAreEqualWhenNameIsEqual()
    {
        var beverage1 = new NamedBeverage("applejuice");
        var beverage2 = new NamedBeverage("applejuice");
        
        Assert.AreEqual(beverage1, beverage2);
    }
    
    [Test]
    public void TwoNamedBeveragesAreNotEqualWhenNameIsNotEqual()
    {
        var beverage1 = new NamedBeverage("applejuice");
        var beverage2 = new NamedBeverage("orangejuice");
        
        Assert.AreNotEqual(beverage1, beverage2);
    }
}