using System.ComponentModel.Design;
using System.Runtime.InteropServices;
using NUnit.Framework;
using VendingMachine.Application.Coins;

namespace VendinMachineTests;

[TestFixture]
public class CoinTests
{
    [Test]
    public void StaticValueCoinHasValue()
    {
        var coin = new StaticValueCoin(27);

        Assert.AreEqual(27, coin.GetValueInCents());
    }

    [Test]
    public void TwoInstancesOfSameCoinAreEqual()
    {
        var coin1 = new TenCoin();
        var coin2 = new TenCoin();
        
        Assert.AreEqual(coin1, coin2);
    }
    
    [Test]
    public void TwoInstancesOfDifferentCoinAreNotEqual()
    {
        var coin1 = new TenCoin();
        var coin2 = new TwentyCoin();
        
        Assert.AreNotEqual(coin1, coin2);
    }
}