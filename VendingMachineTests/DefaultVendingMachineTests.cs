using NUnit.Framework;
using VendingMachine.Application;
using VendingMachine.Application.Beverages;
using VendingMachine.Application.Coins;
using VendingMachine.Application.Exceptions;
using VendingMachine.Application.Interfaces;

namespace VendinMachineTests;

[TestFixture]
public class DefaultVendingMachineTests
{
    private DefaultVendingMachine _machine;
    
    [SetUp]
    public void Init()
    {
        _machine = new DefaultVendingMachine();
    }
    
    
    [Test]
    public void GetCoinsRetrievesNoCoinsWhenEmpty()
    {
        var coins = _machine.GetCoins();
        
        CollectionAssert.IsEmpty(coins);
    }

    [Test]
    public void AddCoinsAddsCoins()
    {
        var coins = new ICoin[]
        {
            new TenCoin(), new TenCoin()
        };
        
        _machine.AddCoins(coins);
        var result = _machine.GetCoins();

        Assert.AreEqual(2, result[new TenCoin()]);
    }
    
    [Test]
    public void EmptyCoinsRemovesAllCoins()
    {
        _machine.EmptyCoins();
        var result = _machine.GetCoins();

        CollectionAssert.IsEmpty(result);
    }
    
    [Test]
    public void AddCoinsThenRemoveRemovesAllCoins()
    {
        var coins = new ICoin[]
        {
            new TenCoin(), new TenCoin()
        };
        
        _machine.AddCoins(coins);
        _machine.EmptyCoins();
        var result = _machine.GetCoins();

        CollectionAssert.IsEmpty(result);
    }
    
    [Test]
    public void GetBeveragesRetrievesNoBeveragesWhenEmpty()
    {
        var beverages = _machine.GetBeverages();
        
        CollectionAssert.IsEmpty(beverages);
    }

    [Test]
    public void AddBeveragesAddsBeverages()
    {
        var beverages = new IBeverage[]
        {
            new NamedBeverage("orangejuice")
        };
        
        _machine.RegisterBeverage(beverages[0], 100);
        _machine.AddBeverages(beverages);
        var result = _machine.GetBeverages();

        CollectionAssert.AreEquivalent(beverages, result);
    }

    [Test]
    public void CanAddSameBeverageMultipleTimes()
    {
        var orangeJuice = new NamedBeverage("orangejuice");
        var appleJuice = new NamedBeverage("applejuice");

        var machine = new DefaultVendingMachine();

        machine.RegisterBeverage(orangeJuice, 120);
        machine.RegisterBeverage(appleJuice, 150);

        machine.AddBeverages(orangeJuice, orangeJuice, appleJuice);
    }
    
    [Test]
    public void EmptyBeveragesRemovesAllBeverages()
    {
        _machine.EmptyBeverages();
        var result = _machine.GetBeverages();

        CollectionAssert.IsEmpty(result);
    }
    
    [Test]
    public void AddBeveragesThenRemoveRemovesAllBeverages()
    {
        var beverages = new IBeverage[]
        {
            new NamedBeverage("orangejuice")
        };
        
        _machine.RegisterBeverage(beverages[0], 0);
        _machine.AddBeverages(beverages);
        _machine.EmptyBeverages();
        var result = _machine.GetBeverages();

        CollectionAssert.IsEmpty(result);
    }
    
    [Test]
    public void RegisterBeverageFailsWhenBeverageAlreadyRegistered()
    {
        var beverage = new NamedBeverage("applejuice");
        
        _machine.RegisterBeverage(beverage, 100);
        Assert.Throws<BeverageAlreadyRegisteredException>(() =>
        {
            _machine.RegisterBeverage(beverage, 100);
        });
    }
    
    [Test]
    public void BuyBeverageFailsWhenBeverageNotRegistered()
    {
        Assert.Throws<BeverageNotRegisteredException>(() =>
        {
            _machine.Buy(new NamedBeverage("applejuice"), null);
        });
    }
    
    [Test]
    public void BuyBeverageFailsWhenNotEnoughMoneyGiven()
    {
        var beverage = new NamedBeverage("applejuice");
        _machine.RegisterBeverage(beverage, 100);
        
        Assert.Throws<NotEnoughMoneyInsertedException>(() =>
        {
            _machine.Buy(new NamedBeverage("applejuice"), null);
        });
        
        Assert.Throws<NotEnoughMoneyInsertedException>(() =>
        {
            _machine.Buy(new NamedBeverage("applejuice"), new TenCoin());
        });
    }
    
    [Test]
    public void BuyBeverageReturnsBeverageWhenExactMoneyGiven()
    {
        var beverage = new NamedBeverage("applejuice");
        _machine.RegisterBeverage(beverage, 100);
        
        var (result, change) = _machine.Buy(new NamedBeverage("applejuice"), new OneHundredCoin());
        
        Assert.AreEqual(result, beverage);
        CollectionAssert.IsEmpty(change);
    }
    
    [Test]
    public void BuyBeverageReturnsBeverageAndChangeWhenTooMuchMoneyGiven()
    {
        var beverage = new NamedBeverage("applejuice");
        _machine.RegisterBeverage(beverage, 120);

        var presentChange = new ICoin[]
        {
            new OneHundredCoin(),
            new FiftyCoin(),
            new TwentyCoin(),
            new TenCoin()
        };
        _machine.AddCoins(presentChange);
        
        var (result, change) = _machine.Buy(new NamedBeverage("applejuice"), new TwoHundredCoin());
        
        Assert.AreEqual(result, beverage);

        var expectedChange = new ICoin[]
        {
            new FiftyCoin(), new TwentyCoin(), new TenCoin()
        };
        
        CollectionAssert.AreEquivalent(expectedChange, change);
    }
    
    [Test]
    public void BuyBeverageReturnsBeverageAndTooLittleChangeWhenNotEnoughMoneyAvailable()
    {
        var beverage = new NamedBeverage("applejuice");
        _machine.RegisterBeverage(beverage, 120);

        var presentChange = new ICoin[]
        {
            new OneHundredCoin(),
            new FiftyCoin(),
            new TwentyCoin(),
        };
        _machine.AddCoins(presentChange);
        
        var (result, change) = _machine.Buy(beverage, new TwoHundredCoin());
        
        Assert.AreEqual(result, beverage);

        var expectedChange = new ICoin[]
        {
            new FiftyCoin(), new TwentyCoin()
        };
        
        CollectionAssert.AreEquivalent(expectedChange, change);
    }
}