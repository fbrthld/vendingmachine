using VendingMachine.Application.Coins;
using VendingMachine.Application.Exceptions;
using VendingMachine.Application.Interfaces;

namespace VendingMachine.Application;

public class DefaultVendingMachine: IVendingMachine
{
    protected Dictionary<ICoin, int> CoinStorage;
    protected Dictionary<IBeverage, int> BeverageStorage;
    protected Dictionary<IBeverage, int> BeveragePricesInCents;

    public DefaultVendingMachine()
    {
        CoinStorage = new Dictionary<ICoin, int>();
        BeverageStorage = new Dictionary<IBeverage, int>();
        BeveragePricesInCents = new Dictionary<IBeverage, int>();
    }

    public IDictionary<ICoin, int> GetCoins()
    {
        return CoinStorage;
    }

    public void AddCoins(params ICoin[] coins)
    {
        foreach (var coin in coins)
        {
            if (CoinStorage.TryGetValue(coin, out var amount))
            {
                CoinStorage[coin] = amount + 1;
            }
            else
            {
                CoinStorage[coin] = 1;
            }
        }
    }
    
    public void RemoveCoins(params ICoin[] coins)
    {
        foreach (var coin in coins)
        {
            if (CoinStorage.TryGetValue(coin, out var amount))
            {
                CoinStorage[coin] = amount - 1;
            }
        }
    }

    public void EmptyCoins(params ICoin[] coins)
    {
        CoinStorage = new Dictionary<ICoin, int>();
    }

    public IEnumerable<IBeverage> GetBeverages()
    {
        return BeverageStorage.Keys;
    }

    public void RegisterBeverage(IBeverage beverage, int priceInCents)
    {
        if (BeveragePricesInCents.TryGetValue(beverage, out var price))
        {
            throw new BeverageAlreadyRegisteredException(beverage);
        }

        BeveragePricesInCents.Add(beverage, priceInCents);
    }

    public void UnregisterBeverage(IBeverage beverage)
    {
        BeveragePricesInCents.Remove(beverage);
    }

    public void AddBeverages(params IBeverage[] beverages)
    {
        foreach (var beverage in beverages)
        {
            // check if beverages are already registered
            FindRegisteredBeverages(beverage);
        }
        
        foreach (var beverage in beverages)
        {
            if (BeverageStorage.TryGetValue(beverage, out var amount))
            {
                BeverageStorage[beverage] = amount + 1;
            }
            else
            {
                BeverageStorage.Add(beverage, 1);
            }
        }
    }

    public void EmptyBeverages(params IBeverage[] beverages)
    {
        BeverageStorage = new Dictionary<IBeverage, int>();
    }

    public (IBeverage, IEnumerable<ICoin>) Buy(IBeverage beverage, params ICoin[] coins)
    {
        var result = FindRegisteredBeverages(beverage);
        
        var totalCents = CountMoney(coins);
        var price = BeveragePricesInCents[beverage];
        if (price > totalCents)
        {
            throw new NotEnoughMoneyInsertedException(totalCents, price);
        }

        AddCoins(coins);
        
        var remaining = totalCents - price;
        var change = CalculateChange(remaining);

        return (beverage, change);
    }

    private IEnumerable<ICoin> CalculateChange(int changeAmount)
    {
        var availableCoins = CoinStorage.Where(pair => pair.Value > 0).ToList();
        if (availableCoins.Count == 0)
            return new ICoin[] { };
        
        var smallestAvailableCoin = availableCoins.MinBy(pair => pair.Key.GetValueInCents()).Key;

        if (smallestAvailableCoin.GetValueInCents() > changeAmount)
            return new ICoin[] { };

        var coinsThatFit = availableCoins.Where(pair => pair.Key.GetValueInCents() <= changeAmount);
        var largestCoinThatFits = coinsThatFit.MaxBy(pair => pair.Key.GetValueInCents()).Key;

        var returnValue = new List<ICoin>();
        returnValue.Add(largestCoinThatFits);
        RemoveCoins(largestCoinThatFits);
        
        returnValue.AddRange(CalculateChange(changeAmount - largestCoinThatFits.GetValueInCents()));
        return returnValue;
    }

    private static int CountMoney(ICoin[] coins)
    {
        if (coins is null)
            return 0;
        
        var totalCents = 0;
        foreach (var coin in coins)
        {
            totalCents += coin.GetValueInCents();
        }

        return totalCents;
    }

    protected IBeverage FindRegisteredBeverages(IBeverage beverage)
    {
        var result = BeveragePricesInCents.Keys.FirstOrDefault(x => Equals(x, beverage));
        if (result is null)
        {
            throw new BeverageNotRegisteredException(beverage.GetIdentifier());
        }
        
        return beverage;
    }
}