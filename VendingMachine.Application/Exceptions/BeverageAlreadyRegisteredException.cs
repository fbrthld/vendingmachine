using VendingMachine.Application.Interfaces;

namespace VendingMachine.Application.Exceptions;

public class BeverageAlreadyRegisteredException: Exception
{
    public BeverageAlreadyRegisteredException(IBeverage beverage): base($"Beverage {beverage.GetIdentifier()} was already registered")
    {
        
    }
}