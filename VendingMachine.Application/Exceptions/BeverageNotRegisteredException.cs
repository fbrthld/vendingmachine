using VendingMachine.Application.Interfaces;

namespace VendingMachine.Application.Exceptions;

public class BeverageNotRegisteredException: Exception
{
    public BeverageNotRegisteredException(string beverage): base($"Beverage {beverage} is not registered")
    {
        
    }
}