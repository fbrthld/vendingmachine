namespace VendingMachine.Application.Exceptions;

public class NotEnoughMoneyInsertedException: Exception
{
    public NotEnoughMoneyInsertedException(int moneyGiven, int moneyExpected): base($"{moneyGiven} cents were given, {moneyExpected} cents were expected")
    {
        
    }
}