using VendingMachine.Application.Interfaces;

namespace VendingMachine.Application.Beverages;

public class NamedBeverage : IBeverage
{
    private readonly string _name;

    public NamedBeverage(string name)
    {
        _name = name;
    }

    public string GetIdentifier()
    {
        return _name;
    }

    public override bool Equals(object? obj)
    {
        if (obj is NamedBeverage coin)
            return coin.GetIdentifier() == GetIdentifier();

        return false;
    }

    protected bool Equals(NamedBeverage other)
    {
        return _name == other._name;
    }

    public override int GetHashCode()
    {
        return _name.GetHashCode();
    }
}