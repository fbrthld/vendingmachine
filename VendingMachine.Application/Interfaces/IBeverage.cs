namespace VendingMachine.Application.Interfaces;

public interface IBeverage
{
    // Identifier could be name, number of slot in machine, etc.
    public string GetIdentifier();
}