namespace VendingMachine.Application.Interfaces;

public interface IVendingMachine
{
    /// <summary>
    /// Returns the coins present in the machine
    /// </summary>
    /// <returns>all coins</returns>
    public IDictionary<ICoin, int> GetCoins();
    
    /// <summary>
    /// Adds new change to the machine
    /// </summary>
    /// <param name="coins"></param>
    public void AddCoins(params ICoin[] coins);
    
    /// <summary>
    /// Removes all the coins from the machine
    /// </summary>
    /// <param name="coins"></param>
    public void EmptyCoins(params ICoin[] coins);
    
    /// <summary>
    /// Gets the types of beverages currently in the machine
    /// </summary>
    /// <returns></returns>
    public IEnumerable<IBeverage> GetBeverages();
    
    /// <summary>
    /// Registers a new beverage and its price
    /// </summary>
    /// <param name="beverage"></param>
    /// <param name="priceInCents"></param>
    public void RegisterBeverage(IBeverage beverage, int priceInCents);
    
    /// <summary>
    /// Unregisters beverage prices
    /// </summary>
    /// <param name="beverage"></param>
    public void UnregisterBeverage(IBeverage beverage);
    
    /// <summary>
    /// Adds a beverage to the machine, requires registration first
    /// </summary>
    /// <param name="beverages"></param>
    public void AddBeverages(params IBeverage[] beverages);
    
    /// <summary>
    /// Removes all beverages from the machine
    /// </summary>
    /// <param name="beverages"></param>
    public void EmptyBeverages(params IBeverage[] beverages);

    /// <summary>
    /// Checks and edits inventory and returns change
    /// </summary>
    /// <param name="beverage"></param>
    /// <param name="coins"></param>
    /// <returns>the beverage and the change</returns>
    public (IBeverage, IEnumerable<ICoin>) Buy(IBeverage beverage, params ICoin[] coins);
}