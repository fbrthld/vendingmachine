namespace VendingMachine.Application.Interfaces;

public interface ICoin
{
    public int GetValueInCents();
}