using VendingMachine.Application.Interfaces;

namespace VendingMachine.Application.Coins;

public class FiftyCoin: StaticValueCoin
{
    public FiftyCoin() : base(50)
    {
    }
}