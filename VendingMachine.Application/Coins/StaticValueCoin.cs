using VendingMachine.Application.Interfaces;

namespace VendingMachine.Application.Coins;

public class StaticValueCoin: ICoin
{
    protected int Value = 0;

    public StaticValueCoin(int value)
    {
        Value = value;
    }

    public int GetValueInCents()
    {
        return Value;
    }

    public override bool Equals(object? obj)
    {
        if (obj is StaticValueCoin coin)
            return coin.GetValueInCents() == GetValueInCents();

        return false;
    }

    protected bool Equals(StaticValueCoin other)
    {
        return Value == other.Value;
    }

    public override int GetHashCode()
    {
        return Value;
    }
}