using VendingMachine.Application.Interfaces;

namespace VendingMachine.Application.Coins;

public class TwentyCoin: StaticValueCoin
{
    public TwentyCoin() : base(20)
    {
    }
}