using VendingMachine.Application.Interfaces;

namespace VendingMachine.Application.Coins;

public class TenCoin: StaticValueCoin
{
    public TenCoin() : base(10)
    {
    }
}