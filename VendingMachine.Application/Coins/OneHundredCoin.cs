using VendingMachine.Application.Interfaces;

namespace VendingMachine.Application.Coins;

public class OneHundredCoin: StaticValueCoin
{
    public OneHundredCoin() : base(100)
    {
    }
}