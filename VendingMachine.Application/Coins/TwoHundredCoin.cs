using VendingMachine.Application.Interfaces;

namespace VendingMachine.Application.Coins;

public class TwoHundredCoin: StaticValueCoin
{
    public TwoHundredCoin() : base(200)
    {
    }
}